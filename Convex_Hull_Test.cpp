// ConvexHullTest.cpp

#include <iostream>
#include <cmath>
#include <fstream>

#include "src/algorithms/3D_Convex_Hull.h"
#include "src/algorithms/Graham_Scan.h"
#include "src/algorithms/Brute_Force.h"
#include "src/algorithms/Jarvis_March.h"
#include "src/algorithms/Quickhull.h"

#include "easyunit/test.h"
#include "easyunit/testharness.h"

#define N_TESTS 4

#include <cstdlib>
#include <string>
#include <random>
#include <cmath>

using namespace easyunit;

g_2d_point my_pivot(0, 0);

bool my_angle_compare(g_2d_point a, g_2d_point b)
{
    if(are_collinear(b, my_pivot, a))
        return distance_2_points(my_pivot, a) < distance_2_points(my_pivot, b);
    double d1x = a.x - my_pivot.x, d1y = a.y - my_pivot.y, d2x = b.x - my_pivot.x, d2y = b.y - my_pivot.y;
    return (atan2(d1y, d1x) - atan2(d2y, d2x)) > 0;
}

double perimeter(const vector<g_2d_point> &poly)
{
    double result = 0;
    for(int i = 0;i < (int)poly.size() - 1;i++)
        result += distance_2_points(poly[i], poly[i + 1]);
    return result;
}

bool is_convex(const vector<g_2d_point> &poly)
{
    int s = (int)poly.size();
    if(s <= 3) return false;
    bool l = is_on_the_left(poly[2], poly[0], poly[1]);
    for(int i = 1; i < s - 1; i++)
        if(is_on_the_left(poly[(i + 2) == s ? 1 : i + 2], poly[i], poly[i + 1]) != l) return false;
    return true;
}

vector<g_2d_point> sort_vector(vector<g_2d_point> &P)
{
        int P0 = 0;
        int n = P.size();
        for(int i = 1; i < n; i++)
        if(P[i].y < P[P0].y || (P[i].y == P[P0].y && P[i].x > P[P0].x))
            P0 = i;
        g_2d_point temp = P[0];
        P[0] = P[P0];
        P[P0] = temp;    
        my_pivot = P[0];
        sort(++P.begin(), P.end(), my_angle_compare);
        vector<g_2d_point> to_R;
        to_R.push_back(P[0]);
        for (int i = 1; i<n-1; i++)
        {
            if (!are_collinear(P[i-1], P[i], P[i+1]))
                to_R.push_back(P[i]);
        }
        to_R.push_back(P[n-1]);
        return to_R;
}

vector<g_2d_point> set_ofg_2d_points(int select)
{
    vector<g_2d_point> S;
    if (select == 0)
    {
        vector<double> x, y;
        S.push_back(g_2d_point(1, 1));
        S.push_back(g_2d_point(1, 3));
        S.push_back(g_2d_point(3, 1));
        S.push_back(g_2d_point(3, 3));
        S.push_back(g_2d_point(2, 2));
    }
    if (select == 1)
    {
        vector<double> x, y;
        S.push_back(g_2d_point(0, 0));
        S.push_back(g_2d_point(0, 1));
        S.push_back(g_2d_point(0, 2));
        S.push_back(g_2d_point(0, 3));
        S.push_back(g_2d_point(0, 4));
        S.push_back(g_2d_point(1, 0));
        S.push_back(g_2d_point(1, 1));
        S.push_back(g_2d_point(1, 2));
        S.push_back(g_2d_point(1, 3));
        S.push_back(g_2d_point(1, 4));
        S.push_back(g_2d_point(2, 0));
        S.push_back(g_2d_point(2, 1));
        S.push_back(g_2d_point(2, 2));
        S.push_back(g_2d_point(2, 3));
        S.push_back(g_2d_point(2, 4));
        S.push_back(g_2d_point(3, 0));
        S.push_back(g_2d_point(3, 1));
        S.push_back(g_2d_point(3, 2));
        S.push_back(g_2d_point(3, 3));
        S.push_back(g_2d_point(3, 4));
        S.push_back(g_2d_point(4, 0));
        S.push_back(g_2d_point(4, 1));
        S.push_back(g_2d_point(4, 2));
        S.push_back(g_2d_point(4, 3));
        S.push_back(g_2d_point(4, 4));
    }
    if (select == 2)
    {
        random_device rd;
        mt19937 gen(rd());
        normal_distribution<> d(0,3);
        for( int i=0; i< 10; i++)
        {
            S.push_back(g_2d_point(d(gen),d(gen)));
        }
    }
    if (select == 3)
    {
        random_device rd;
        mt19937 gen(rd());
        normal_distribution<> d(0,3);
        for( int i=0; i< 100; i++)
        {
            S.push_back(g_2d_point(d(gen),d(gen)));
        }
    }
    return S;
}

TEST(Brute_Force,test)
{
    vector<g_2d_point> cv;
    for (int i=0; i< N_TESTS; i++)
    {
        vector<g_2d_point> S = set_ofg_2d_points(i);
        cv = Brute_Force(S);
        cv = sort_vector(cv);
        cv.push_back(g_2d_point(cv[0].x, cv[0].y));
        ASSERT_TRUE(is_convex(cv));

        vector<g_2d_point> ncv;
        ncv = Graham_Scan(S);
        ncv = sort_vector(ncv);
        ncv.push_back(g_2d_point(ncv[0].x, ncv[0].y));

        ASSERT_TRUE(abs(perimeter(cv) - perimeter(ncv)) < EPS);
    }
}

TEST(Jarvis_March,test)
{
    vector<g_2d_point> cv;
    for (int i=0; i< N_TESTS; i++)
    {
        vector<g_2d_point> S = set_ofg_2d_points(i);
        cv = Jarvis_March(S);
        cv = sort_vector(cv);
        cv.push_back(g_2d_point(cv[0].x, cv[0].y));

        ASSERT_TRUE(is_convex(cv));

        vector<g_2d_point> ncv;
        ncv = Graham_Scan(S);
        ncv = sort_vector(ncv);
        ncv.push_back(g_2d_point(ncv[0].x, ncv[0].y));
        ASSERT_TRUE(abs(perimeter(cv) - perimeter(ncv)) < EPS);
    }
}

TEST(Quickhull,test)
{
    vector<g_2d_point> cv;
    for (int i=0; i< N_TESTS; i++)
    {
        vector<g_2d_point> S = set_ofg_2d_points(i);
        cv = Quickhull(S);
        cv = sort_vector(cv);
        cv.push_back(g_2d_point(cv[0].x, cv[0].y));

        ASSERT_TRUE(is_convex(cv));

        vector<g_2d_point> ncv;
        ncv = Graham_Scan(S);
        ncv = sort_vector(ncv);
        ncv.push_back(g_2d_point(ncv[0].x, ncv[0].y));
        ASSERT_TRUE(abs(perimeter(cv) - perimeter(ncv)) < EPS);
    }
}

TEST(Graham_Scan,test)
{
    vector<g_2d_point> cv;
    for (int i=0; i< N_TESTS; i++)
    {
        vector<g_2d_point> S = set_ofg_2d_points(i);
        cv = Graham_Scan(S);
        cv = sort_vector(cv);
        cv.push_back(g_2d_point(cv[0].x, cv[0].y));

        ASSERT_TRUE(is_convex(cv));

        vector<g_2d_point> ncv;
        ncv = Jarvis_March(S);
        ncv = sort_vector(ncv);
        ncv.push_back(g_2d_point(ncv[0].x, ncv[0].y));
        ASSERT_TRUE(abs(perimeter(cv) - perimeter(ncv)) < EPS);
    }
}

bool is_in(g_3d_point p, vector<g_3d_point> s)
{
    if(count(s.begin(), s.end(), p) > 0)
        return true;
    else
        return false;
}

TEST(CV3D,test)
{
    vector<g_3d_point> s;
    s.push_back(g_3d_point(0, 0, 0));
    s.push_back(g_3d_point(0, 0, 2));
    s.push_back(g_3d_point(0, 2, 0));
    s.push_back(g_3d_point(0, 2, 2));
    s.push_back(g_3d_point(2, 0, 0));
    s.push_back(g_3d_point(2, 0, 2));
    s.push_back(g_3d_point(2, 2, 0));
    s.push_back(g_3d_point(2, 2, 2));
    s.push_back(g_3d_point(1, 1, 1));
    vector<g_3d_point> cv = Convex_Hull_3D(s);
    ASSERT_TRUE(is_in(g_3d_point(0, 0, 0), cv));
    ASSERT_TRUE(is_in(g_3d_point(0, 0, 2), cv));
    ASSERT_TRUE(is_in(g_3d_point(0, 2, 0), cv));
    ASSERT_TRUE(is_in(g_3d_point(0, 2, 2), cv));
    ASSERT_TRUE(is_in(g_3d_point(2, 0, 0), cv));
    ASSERT_TRUE(is_in(g_3d_point(2, 0, 2), cv));
    ASSERT_TRUE(is_in(g_3d_point(2, 2, 0), cv));
    ASSERT_TRUE(is_in(g_3d_point(2, 2, 2), cv));
    ASSERT_TRUE(!is_in(g_3d_point(1, 1, 1), cv));
}

int main() {
  TestRegistry::runAndPrint();
} 
